#!/bin/bash
which nvm
if [ $? -ne 0 ]; then
    export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    source $NVM_DIR/nvm.sh
fi
set -e
export NODE_OPTIONS=--openssl-legacy-provider
nvm use --lts
#npm run build
npm run pack-prod -- deb 

